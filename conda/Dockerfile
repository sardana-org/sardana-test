FROM ghcr.io/mamba-org/micromamba:1.5.8

USER root

ENV DEBIAN_FRONTEND noninteractive
RUN apt update -y \
    && apt install --no-install-recommends -y \
      xvfb xauth libpci3 libxrender1 libxtst6 \
    && rm -rf /var/lib/apt/lists/*

USER mambauser

ARG PYTHON_VERSION=3.10
ARG PYTANGO_VERSION=9.3.6

RUN micromamba install --yes --name base --channel conda-forge \
      python=$PYTHON_VERSION  \
      pytango=$PYTANGO_VERSION  \
      pytest \
      pytest-xvfb \
      h5py \
      matplotlib \
      pyqtgraph \
      pymca \
      qtconsole \
      itango \
      taurus \
      dsconfig \
      "ruamel.yaml<=0.18.5" \
      "pydantic<2.0" \
      jsonpatch \
      python-graphviz \
    && micromamba install --yes --name base --channel conda-forge --only-deps sardana-core \
    # Install sardana with pip so we can rebuild the image without waiting for conda-forge package
    && /opt/conda/bin/python3 -m pip install --no-dependencies --no-cache-dir sardana \
    && micromamba clean --all --yes \
    && find /opt/conda -follow -type f -name '*.a' -delete

ENV PATH=/opt/conda/bin:$PATH
